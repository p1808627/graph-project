from sage.graphs.graph_plot import GraphPlot

class Trigraph():

	"""	constructor of trigraph
			allEdgesRed=True means that the trigraph contains only red edges
	"""
	def __init__(self, current_graph, black_graph = None, red_graph = None, partition= None, allEdgesRed=False):
		self.current_graph = current_graph
		n = current_graph.num_verts()

		if(black_graph == None and red_graph == None and partition == None):
			copyOfCurrent = Graph(n, loops=False, multiedges=False)
			for i in range(n):
				for j in range(i+1, n):	
					if current_graph.has_edge(current_graph.vertices()[i],current_graph.vertices()[j]):
						copyOfCurrent.add_edge(i,j)
			
			emptyGraph = Graph(n, loops=False, multiedges=False)

			self.black_graph = copyOfCurrent if not allEdgesRed else emptyGraph
			self.red_graph = emptyGraph if not allEdgesRed else copyOfCurrent

			self.partition = []
			i = 0
			while(i < n):
				self.partition.append(set([current_graph.vertices()[i]]))
				i = i + 1
		else :
			self.black_graph = black_graph
			self.red_graph = red_graph
			self.partition = partition

	#to be tested	
	def getKBestPair(self):
		i = 0
		tab = []
		minimum = sizeSymDif(self.black_graph, 0, 1) + sizeUnionNeighbors(self.red_graph,0,1)
		paire = (0,1)
		tab.append(paire);
		while(i < self.black_graph.num_verts()):
			j = i + 1
			while(j < self.black_graph.num_verts()):
				newMin = sizeSymDif(self.black_graph, i, j) + sizeUnionNeighbors(self.red_graph,i,j)
				print(minimum)
				print(newMin)
				if(minimum == newMin):
					paire = (i,j)
					tab.append(paire)
				if(minimum > newMin):
					tab[:] = []
					paire = (i,j)
					tab.append(paire)
					minimum = newMin
				j = j + 1
			i= i + 1
		return tab


	def getBestPair(self):
		return min([(i, j) for i in range(self.black_graph.num_verts()) for j in range(i+1, self.black_graph.num_verts())], key=lambda pair : self.score(pair[0], pair[1]))

	def getLabeledBestPair(self):
		bestPair = self.getBestPair()
		return (self.partition[bestPair[0]], self.partition[bestPair[1]])


	#the new red degree of vertex v if i and j are contracted
	#v is assumed to be different from i and j
	def newRedDegree(self, i, j, v):
		redNeigh = self.red_graph.neighbors(v)
		redDeg = len(redNeigh)
		for x in redNeigh:
			if x==i or x==j:
				redDeg -= 1

		if self.black_graph.has_edge(i, v) != self.black_graph.has_edge(j, v) or self.red_graph.has_edge(i, v) or self.red_graph.has_edge(j, v):
			redDeg += 1
		return redDeg

	#the twinwidth of the trigraph if i and j are contracted
	def score(self, i, j):
		scoreMax = 0
		scoreij = 0
		for v in self.black_graph.vertices():			
			if v != i and v != j:
				newRedDegv = self.newRedDegree(i, j, v)
				if newRedDegv > scoreMax:
					scoreMax = newRedDegv
				if self.black_graph.has_edge(i, v) != self.black_graph.has_edge(j, v) or self.red_graph.has_edge(i, v) or self.red_graph.has_edge(j, v):
						scoreij += 1
					
		return max(scoreij, scoreMax)
						

	""" contracte un graph puis retourne un nouveau trigraph avec
	le black_graph, red_graph et la partition mis à jour"""
	def contract(self):
		pair = self.getBestPair()
		newRedGraph = self.red_graph.copy()
		newBlackGraph = self.black_graph.copy()
		for v in self.black_graph.vertices(sort=False):
			if v != pair[0] and v != pair[1]:
				if self.black_graph.has_edge(v, pair[0]) != self.black_graph.has_edge(v, pair[1]):
					newBlackGraph.delete_edge(v, pair[0])
					newRedGraph.add_edge(v, pair[0])
		newBlackGraph.delete_vertex(pair[1])
		newBlackGraph.relabel({i : i-1 for i in range(pair[1]+1,self.black_graph.num_verts())})
		newRedGraph.delete_vertex(pair[1])
		newRedGraph.relabel({i : i-1 for i in range(pair[1]+1,self.red_graph.num_verts())})
		newPartition = self.partition.copy()
		newPartition[pair[0]] = newPartition[pair[0]].union(newPartition[pair[1]])
		del newPartition[pair[1]]
		t = Trigraph(self.current_graph,newBlackGraph,newRedGraph,newPartition)
		return t
				
			

	def contractOld(self):
		vertice = self.black_graph.vertices(sort=False)
		paire = self.getBestPair()
		paire1 = vertice[paire[0]]
		paire2 = vertice[paire[1]]
		newRedGraph = self.red_graph.copy()
		newBlackGraph = self.black_graph.copy()
		voisin1 = newBlackGraph.neighbors(paire1)
		voisin1.append(paire1)
		voisin2 = newBlackGraph.neighbors(paire2)
		voisin2.append(paire2)
		newBlackGraph.delete_vertices([paire1,paire2])
		newBlackGraph.add_vertices([paire1])
		newRedGraph.delete_vertices([paire1,paire2])
		newRedGraph.add_vertices([paire1])
		ajouterLienNoir(newBlackGraph,paire1,paire2,voisin1,voisin2)
		ajouterLienRouge(newRedGraph,paire1,paire2,voisin1,voisin2)
		completerRedGraph(newRedGraph,paire1,paire2,voisin1,voisin2)
		newPartition = self.partition.copy()
		newPartition[paire[0]] = newPartition[paire[0]].union(newPartition[paire[1]])
		del newPartition[paire[1]]
		t = Trigraph(self.current_graph,newBlackGraph,newRedGraph,newPartition)
		return t
		""" creer une instanc de Trigraph"""


	def show(self):
		graphe = self.black_graph.copy()
		vertice = self.red_graph.vertices(sort=False)
		i = 0

		while(i < len(vertice)):
			if(self.red_graph.neighbors(vertice[i]) != []):
				j = 0
				v = self.red_graph.neighbors(vertice[i])
				while (j < len(v)):
					graphe.add_edge(vertice[i],v[j],'red')
					j = j + 1
			i = i + 1
		j = 0
		while(j < len(vertice)):
			graphe.relabel({vertice[j]: str(self.partition[j])})
			j = j + 1
		graphe = graphe.plot(color_by_label={'red' : 'red' , '' : 'black'})
		show(graphe)
		

	"""Donne le chiffre qui se rapproche d'un cograph"""
	def twinwidth(self):
		return max(self.red_graph.degree())
