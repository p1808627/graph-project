class ContractionSequence():


	def __init__(self,graph,liste = None):
		if(liste == None):
			self.liste = []

		self.graph = Trigraph(graph) if type(graph) is Graph else graph
		self.twinwidth = 0	
		
	def buildSequence(self):
		while(len(self.graph.black_graph.vertices()) > 1):
			self.graph = self.graph.contract()
			#self.graph.show()
			self.ajout()
			if (self.graph.twinwidth() > self.twinwidth):
				self.twinwidth = self.graph.twinwidth()
		#print("tww = ", self.twinwidth)
		return self.twinwidth
	

	def ajout(self):
		self.liste.append(self.graph)

