#returns the symmetric difference of the neighborhoods of vertices number i and j in g
def sizeSymDif(g, i, j):
	return Set(g.neighbors(g.vertices()[i])).symmetric_difference(Set(g.neighbors(g.vertices()[j]))).difference({i, j}).cardinality()

#given a graph g, returns a pair (i, j) of vertex indices with minimum symmetric difference of neighborhoods
def getBestPair(g):
	return min([(i, j) for i in range(g.num_verts()) for j in range(i+1, g.num_verts())], key=lambda pair : sizeSymDif(g, pair[0], pair[1]))

	


