

def vérificationTableau(tab1,tab2):
	i = 0
	boolean = True
	if (len(tab1)!=len(tab2)):
		return False
	while (i < len(tab1) and boolean == True):
		u = 0
		while(u < len(tab2)):
			if(tab1[i]!=tab2[u]):
				boolean = False
				u = u+1
			else:
				u = len(tab2)
				boolean = True
		i = i+1
	return boolean 


def NbélémentNonPrésent(tab1,tab2):
	val = 0
	i= 0
	while(i < len(tab1)):
		j=0
		boolean = False
		while( j < len(tab2) and boolean == False):
			if(tab1[i] == tab2[j]):
				boolean = True
			else :
				boolean = False
			j = j+1
		if(boolean == False):
			val = val + 1
		i = i +1 
	return val



def trouverElement(elem,tab2):
	i = 0
	while(i<len(tab2)):
		if(elem == tab2[i]):
			return True
		i = i + 1
	return False


def test(self):
	NbCographe = 0
	NbElemDif = 0
	boolean = False
	M = self.common_neighbors_matrix(nonedgesonly = False)
	vertices = self.vertices()
	common_neighbors = self.most_common_neighbors(nonedgesonly = False)
	print(len(vertices))
	while(len(vertices) > 3):
		j = 0
		boolean = False
		vertices = self.vertices()
		print(vertices)
		common_neighbors = self.most_common_neighbors(nonedgesonly = False)
		print(common_neighbors)
		for u in range (len(common_neighbors)):
			if(boolean == False):
				i =0
				tab1 = self.vertex_boundary([common_neighbors[u][i]])
				print(tab1)
				tab1.append(common_neighbors[u][i])
				print("\n")
				print(tab1)
				tab2 = self.vertex_boundary([common_neighbors[u][i+1]])
				tab2.append(common_neighbors[u][i+1])
				print("\n")
				print(tab2)
				if(trouverElement(tab1[-1],tab2)): 

					if(vérificationTableau(tab1,tab2)):
						print("True")
						boolean = True
					else :
						print("False")
						boolean = False
				else :
					print("pas compatible")
					boolean = False
		if(boolean == True):
			self.delete_vertices([tab1[-1],tab2[-1]])
			vertice_name = str(tab2[-1]) + str(tab1[-1])
			self.add_vertices([int(vertice_name)])
			print(len(tab1))
			while(j < len(tab1)-1):
				if(tab1[j] != tab2[-1]):
					self.add_edge(int(vertice_name),tab1[j])
					print("fin")
				j = j+1
		if(boolean == False):
			while(trouverElement(tab1[-1],tab2) == False and j < len(vertices)):
				print("boucle")
				u = 0
				while(u < len(vertices) and boolean == False):
					print("boucle2")
					print(vertices)
					tab1 = self.vertex_boundary([vertices[j]])
					tab2 = self.vertex_boundary([vertices[u]])
					print(tab1)
					print(tab2)
					if(trouverElement(vertices[j],tab2)):
						print("sommet compatible")
						tab1.append(vertices[j])
						tab2.append(vertices[u])
						boolean = True
					u = u + 1
				j = j + 1
			j = 0
			print(tab1)
			print(tab2)
			self.delete_vertices([tab1[-1],tab2[-1]])
			vertice_name = str(tab2[-1]) + str(tab1[-1])
			self.add_vertices([int(vertice_name)])
			NbElemDif = NbElemDif + NbélémentNonPrésent(tab1,tab2)
			NbElemDif = NbElemDif + NbélémentNonPrésent(tab2,tab1)
			print(NbElemDif)
			print(NbCographe)
			if (NbCographe < NbElemDif):
				NbCographe = NbElemDif
				NbElemDif = 0
			while(j < len(tab1)-1):
				if(tab1[j] != tab2[-1]):
					self.add_edge(int(vertice_name),tab1[j])
				j = j +1
			j = 0
			while(j < len(tab2)):
				boolean1 = False
				v = 0
				while(v < len(tab1) and boolean1 == False):
					if(tab2[j] != tab1[v]):
						boolean1 = False
					else :
						boolean1 = True
					v = v +1
				if(boolean1 == False):
					self.add_edge(int(vertice_name),tab2[j])
				j = j +1
	vertices = self.vertices()
	print(vertices)
	self.delete_vertices([vertices[0],vertices[1]])
	vertice_name = str(vertices[0]) + str(vertices[1])
	self.add_vertices([int(vertice_name)])
	show(self)
	return NbCographe

def NbCographe(self):
	s = "nonPaire"
	valeurRetour = 0
	sommet = self.vertices()
	if(sommet[0] != 0):
		s = "pair"
	u = 0
	if(self.is_cograph()):
		return 0
	while(len(sommet) > 2):
		i = 0
		sommet = self.vertices()
		NbCograph = NbélémentNonPrésent(self.neighbors(sommet[0]),self.neighbors(sommet[1]))
		NbCograph = NbCograph + NbélémentNonPrésent(self.neighbors(sommet[1]),self.neighbors(sommet[0]))
		
		boolean = False
		while (i < len(sommet)-1):
			j = i+1
			while(j < len(sommet)):
				voisin = self.neighbors(sommet[i])
				voisin2 = self.neighbors(sommet[j])
				nbVoisinsDiff = NbélémentNonPrésent(voisin,voisin2)
				nbVoisinsDiff = nbVoisinsDiff + NbélémentNonPrésent(voisin2,voisin)
				if (NbCograph > nbVoisinsDiff):
					boolean = True
					NbCograph = nbVoisinsDiff
					voisinMin = voisin
					voisinMin.append(sommet[i])
					voisinMin2 = voisin2
					voisinMin2.append(sommet[j])
					nbVoisinsDiff = 0
				j = j + 1
			i= i + 1
		if(boolean == False):
			voisinMin = self.neighbors(sommet[0])
			voisinMin.append(sommet[0])
			voisinMin2 = self.neighbors(sommet[1])
			voisinMin2.append(sommet[1])
		if(s == "nonPaire"):
			self.delete_vertices([voisinMin[-1],voisinMin2[-1]])
			nom_sommet = str(voisinMin2[-1]) + str(voisinMin[-1])
			self.add_vertices([int(nom_sommet)])
		if(s == "pair"):
			self.delete_vertices([voisinMin[-1],voisinMin2[-1]])
			print([voisinMin[-1],voisinMin2[-1]])
			nom_sommet = voisinMin[-1]
			self.add_vertices([nom_sommet])
		k = 0
		while(k < len(voisinMin)-1):
			if(s == "nonPaire"):
				self.add_edge(int(nom_sommet),voisinMin[k])
			if(s == "pair"):
				self.add_edge(nom_sommet,voisinMin[k])
			k = k + 1
		k = 0
		while(k < len(voisinMin2)-1):
			boolean = False
			i = 0
			while(i<len(voisinMin) and boolean == False):
				if(voisinMin2[k]!=voisinMin[i]):
					boolean = False
				else:
					boolean = True
				i = i + 1
			if(boolean == False):
				if(s == "nonPaire"):
					self.add_edge(int(nom_sommet),voisinMin2[k])
				if(s == "pair"):
					self.add_edge(nom_sommet,voisinMin2[k])
			k= k + 1
		sommet= self.vertices()
		if(NbCograph > valeurRetour):
			valeurRetour = NbCograph
		u = u +1
	sommet = self.vertices()
	self.delete_vertices([sommet[0],sommet[1]])
	if(s == "nonPaire"):
		nom_sommet = str(sommet[0]) + str(sommet[1])
		self.add_vertices([int(nom_sommet)])
	if(s == "pair"):
		nom_sommet = voisinMin[-1]
		self.add_vertices((nom_sommet))
	show(self)
	return valeurRetour
		





